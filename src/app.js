const express = require('express');
const app = express();

const cities = ["Bordeaux", "Paris", "Lyon", "Strasbourg", "Marseille", "Toulouse"];

//routes
app.get('/status', (req, res) => {
    res.status(200).send();
    res.end();
});

app.get('/city', (req, res) => {
    res.status(200).json(cities);
    res.end();
});

app.use(function(req, res, next) {
    res.status(404).send('404: Page not Found');
    res.end();
});

app.listen(process.env.PORT || 3000, () => {
    console.log(`App started on port ${process.env.PORT || 3000}`);
});
